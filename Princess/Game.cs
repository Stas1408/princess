﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Princess
{
    class Game
    {
        private static bool status = true;
        private static int rows = 12;    // размер поля
        private static int columb = 12;  // размер поля
        private static int hp = 100;
        private static int damage = (rows - 2) * (columb - 2) / 3;
        private static char[,] map1 = new char[rows, columb];
        public static void Main1()
        {  
            
            SetHero();
            FillMap(map1, rows, columb);
            int x = 1, y = 1;          // стартовые координаты
            map1[y, x] = 'X';
            while (status)            
            {
                Console.WriteLine("Уровень здоровья:" + hp);
                MoveHero(ref y, ref x);
                if (map1[(rows - 2), (columb - 2)] == 'X')
                    status = false;
                GetDemage(y, x);
                if (hp <= 0)
                {
                    Console.WriteLine("Вы погибли!!!"); break;
                }
                Console.Clear();
                PrintMap(map1);
            }
            GetEnd();

        }

        public static void SetHero()
        {
            Console.WriteLine("ДОБРО ПОЖАЛОВАТЬ!!!");
            Thread.Sleep(500);
            Console.WriteLine("Выберете режим игры:");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1.Обычный: 100 HP, шанс получения урона - 50%");
            Thread.Sleep(500);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("2.Танк: 150 HP, шанс получения урона - 70%");
            Thread.Sleep(500);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("3.Везунчик: 50HP, шанс получения урона - 30%");
            Thread.Sleep(500);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("НОМЕР ПЕРСОНАЖА");
            bool character = false;
            while (!character)
            {
                string n = Console.ReadLine();
                int n1;
                while (!Int32.TryParse(n, out n1))
                {
                    Console.WriteLine("введено неккоректное значение");
                    Console.WriteLine("Выберите персонажа");
                    n = Console.ReadLine();
                }
                switch (n1)
                {
                    case 1: character = true; damage /= 2; break;
                    case 2: character = true; hp = hp * 3 / 2; damage = damage * 7 / 10; break;
                    case 3: character = true; hp /= 2; damage = damage * 3 / 10; break;
                    default: character = false; Console.WriteLine("ОШИБКА!!!"); break;
                }
            }
            Console.Clear();
        }

        public static void FillMap(char[,] map1, int columbMap, int rowsMap)
        {
            for (int i = 0; i < columbMap; i++)
            {
                for (int j = 0; j < rowsMap; j++)
                {
                    map1[i, j] = ' ';
                    if (i == 0)
                    {
                        map1[i, j] = '#';
                    }
                    if (i == (columbMap - 1))
                    {
                        map1[i, j] = '#';
                    }
                    if (j == 0)
                    {
                        map1[i, j] = '#';
                    }
                    if (j == (rowsMap - 1))
                    {
                        map1[i, j] = '#';
                    }
                    if (j == (rowsMap - 2) && i == (columbMap - 2))
                    {
                        map1[i, j] = '@';
                    }
                    if (j == 1 && i == 1)
                    {
                        map1[i, j] = 'X';
                    }
                    Console.Write(map1[i, j]);
                }
                Console.WriteLine();
            }
        }

        public static void PrintMap(Char[,] map)
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    Console.Write(map[i, j]);
                }
                Console.WriteLine();
            }
        }
        public static void MoveHero(ref int y, ref int x)
        {
            char m = Console.ReadKey(true).KeyChar;
            switch (m)
            {
                case 'd':
                    if (map1[y, x + 1] == '#')
                        break;
                    else
                        map1[y, x] = ' ';
                    map1[y, x + 1] = 'X';
                    x++; break;
                case 'a':
                    if (map1[y, x - 1] == '#')
                        break;
                    else
                        map1[y, x] = ' ';
                    map1[y, x - 1] = 'X';
                    x--; break;
                case 'w':
                    if (map1[y - 1, x] == '#')
                        break;
                    else
                        map1[y, x] = ' ';
                    map1[y - 1, x] = 'X';
                    y--; break;
                case 's':
                    if (map1[y + 1, x] == '#')
                        break;
                    else
                        map1[y, x] = ' ';
                    map1[y + 1, x] = 'X';
                    y++; break;
                default:
                    Console.WriteLine("Некоректное число повторите ввод");
                    break;
            }
        }

        public static void GetDemage(int y,int x)
        {
            Random r;
            r = new Random();
            for (int d = 2; d < damage; d++)
            {
                int i1 = r.Next(2, (rows - 1));
                int j1 = r.Next(1, (columb - 1));
                if (y == i1 && x == j1)
                {
                    hp = hp - 25;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ловушка!");
                    Thread.Sleep(1000);
                    Console.ForegroundColor = ConsoleColor.Magenta;
                }
            }
        }

        public static void GetEnd()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            if (hp > 0)
            {
                Console.WriteLine("ВЫ ПОБЕДИЛИ!!!");
                Console.WriteLine("Уровень здоровья:" + hp);
            }
            else
            {
                Console.WriteLine("Вы погибли!!!");
            }
            Console.ReadKey();
        }
    }
}
